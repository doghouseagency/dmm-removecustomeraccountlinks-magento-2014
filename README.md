# Doghouse Remove Customer Account Links

## Features

- Does what the name implies.

## Installation

Install using Composer (preferred) or Modman.

## Usage

    <customer_account>
        <reference name="customer_account_navigation">
            <action method="removeLinkByName"><name>billing_agreements</name></action>
            <action method="removeLinkByName"><name>recurring_profiles</name></action>
            <action method="removeLinkByName"><name>tags</name></action>
            <action method="removeLinkByName"><name>wishlist</name></action>
            <action method="removeLinkByName"><name>OAuth Customer Tokens</name></action>
            <action method="removeLinkByName"><name>downloadable_products</name></action>
            <action method="removeLinkByName"><name>reviews</name></action>
            <action method="removeLinkByName"><name>newsletter</name></action>
        </reference>
    </customer_account>
